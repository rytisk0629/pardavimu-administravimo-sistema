**Pardavimų Administravimo Sistema**

Pardavimu sistema leidžianti pirkti ir parduoti greitai ir patogiai savo produktus. 
Sistema leidžia lengvai talpinti, redaguoti ir pateikti užsakymus. Visus savo užsakymus galima matyti.


## Funkciniai reikalavimai

1.	Pirkėjas gali prisiregistruoti ir po to prisijungti prie sistemos su savo duomenimis.
2.	Pirkįjas gali rinktis prekes iš katalogo.
3.	Pirkėjas gali palengvinti prekės paiešką filtruodamas prekes pagal kategorijas.
4.	Pirkėjas turi galimybę atliktį pirkinių paiešką pagal prekės pavadinimą.
5.	Pirkėjas gali pasirinktas prekes talpinti pirkinių krepšelyje.
6.	Pirkėjas gali pateiktį užsakimą pagal jo pasirinktas prekes.
7.	Pardavėjas gali talpinti savo prekes parduotuvės prekių kataloge. 
8.	Pardavėjas gauna užsakymų ataskaitą.
9.	Vartotojai gali keisti savo slaptažodį.


## Nefunkciniai reikalavimai: 
1.	Perkamos produkcijos informacija yra kaupiama.
2.	Sistema kontroliuojama administratoriaus.
3.	Sistema užtikrina vartotojų duomenų saugumą.
4.	Sistemoje yra laikomasi visų etikos reikalavimų.
5.	Sistema turi būti lengvai suprantama vartotojams ir joje turi būti lengva naviguoti.
