﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;

namespace PAS
{
    public partial class MyOrders : System.Web.UI.Page
    {
		SqlConnection conn = new SqlConnection("Data Source=(LocalDB)\\MSSQLLocalDB;AttachDbFilename=|DataDirectory|\\PaasDB.mdf;Integrated Security=True");
		string user = "username";

		protected void Page_Load(object sender, EventArgs e)
		{
			conn.Open();
			string sql = "SELECT * FROM orders INNER JOIN products ON [orders].[product] = [products].[Id] WHERE [orders].[user] = '" + user + "'";
			SqlCommand cmd = new SqlCommand(sql, conn);
			cmd.ExecuteNonQuery();
			SqlDataAdapter adapter = new SqlDataAdapter();
			adapter.SelectCommand = cmd;
			DataSet ds = new DataSet();
			adapter.Fill(ds);
			OrdersList.DataSource = ds;
			OrdersList.DataBind();
			conn.Close();
		}
	}
}