﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ProductsCatalog.aspx.cs" Inherits="PAS.ProductsCatalog" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="Styles/ProductsCatalog.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">

        <nav>
            <label class="logo">Pardavimų administravimo sistema</label>
            <ul>
                <li><a class="#" href="#">Home</a></li>
                <li><a href="#">About</a></li>
                <li><a class="active" href="#">Products catalog</a></li>
                <li><a href="#">Authors</a></li>
            </ul>
        </nav>

        <div class="products">
            
			<asp:DataList ID="ProductsData" runat="server" RepeatColumns="5">
                <ItemTemplate>
                    <div class="product">
                        <div class="image">
                            <img src="<%#Eval("Image") %>"/>
                        </div>
                        <div class="description"><%#Eval("Description") %></div>
                        <div class="price">
						    <asp:Button class="purchaseButton" ID="Button" runat="server" Text="Purchase" /> 
                            
                            <%#Eval("Price")%>&euro;
                        </div>
                    </div>
                </ItemTemplate>

			</asp:DataList>

        </div>
    </form>
</body>
</html>
