﻿﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;

namespace PAS
{
	public partial class ShoppingCart : System.Web.UI.Page
	{
		SqlConnection conn = new SqlConnection("Data Source=(LocalDB)\\MSSQLLocalDB;AttachDbFilename=|DataDirectory|\\PaasDB.mdf;Integrated Security=True");
		string user = "username";

		protected void Page_Load(object sender, EventArgs e)
		{
			conn.Open();
			string sql = "SELECT [products].[Id],[products].[description],[products].[price],[shoppingcart].[productId],[shoppingcart].[amount]" +
				" FROM products INNER JOIN shoppingcart ON [shoppingcart].[productId] = [products].[Id] AND [shoppingcart].[user] = '" + user + "'";
			SqlCommand cmd = new SqlCommand(sql, conn);
			cmd.ExecuteNonQuery();
			SqlDataAdapter adapter = new SqlDataAdapter();
			adapter.SelectCommand = cmd;
			DataSet ds = new DataSet();
			adapter.Fill(ds);
			ShoppingCartList.DataSource = ds;
			ShoppingCartList.DataBind();
			conn.Close();
		}

		protected void btn_purchase_Click(object sender, EventArgs e)
		{
			conn.Open();
			string sql = "SELECT * FROM shoppingcart WHERE [user] = '" + user + "'";
			SqlCommand cmd = new SqlCommand(sql, conn);
			cmd.ExecuteNonQuery();
			SqlDataAdapter adapter = new SqlDataAdapter();
			adapter.SelectCommand = cmd;
			DataSet ds = new DataSet();
			adapter.Fill(ds);

			sql = "INSERT INTO orders ([user], [product], [amount], [date]) VALUES ";
			string date = DateTime.Now.ToString("MM/dd/yyyy");
			foreach (DataRow row in ds.Tables[0].Rows)
			{
				sql += "('" + row["user"].ToString().Trim() + "', '" + row["productId"].ToString().Trim() + "', '" + row["amount"].ToString().Trim() + "', '" + date + "'), ";
			}
			sql = sql.Substring(0, sql.Length - 2);

			cmd = new SqlCommand(sql, conn);
			cmd.ExecuteNonQuery();

			sql = "DELETE FROM shoppingcart WHERE [user] = '" + user + "'";
			cmd = new SqlCommand(sql, conn);
			cmd.ExecuteNonQuery();

			conn.Close();

			Page.Response.Redirect(Page.Request.Url.ToString(), true);
		}
	}
}