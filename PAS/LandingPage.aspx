﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LandingPage.aspx.cs" Inherits="PAS.LandingPage" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>PAS</title>
    <link type="text/css" rel="stylesheet" href="Styles/LandingPage.css"/>
</head>
<body>
    <form id="form1" runat="server">
        <nav>
            <label class="logo">LOGO</label>
            <ul>
                <li><a class="active" href="#">Home</a></li>
                <li><a href="#">About</a></li>
                <li><a href="#" onclick="window.open('Login.aspx', '_self');">Login</a></li>
                <li><a href="#" onclick="window.open('Registration.aspx', '_self');">Register</a></li>
                <li><a href="#" onclick="window.open('ShoppingCart.aspx', '_self');">Cart</a></li>
                <li><a href="#" onclick="window.open('MyOrders.aspx', '_self');">My Orders</a></li>
            </ul>
        </nav>

        <div class="search-container">
            <center>
                <asp:TextBox ID="txt_search" CssClass="search-text" runat="server"></asp:TextBox>
                <asp:Button ID="btn_search" class="search-button" name="search" runat="server" Text="Search" OnClick="btn_search_Click"/>
            </center>
        </div>
    </form>
    <div class="showcase">
    </div>
</body>
</html>
