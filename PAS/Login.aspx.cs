﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PAS
{
    public partial class Login : System.Web.UI.Page
    {
        SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=|DataDirectory|PaasDB.mdf;Integrated Security=True");

        protected void Page_Load(object sender, EventArgs e)
        {
            con.Open();
        }

        protected void Login_Button_Click(object sender, EventArgs e)
        {
            SqlCommand com = new SqlCommand("select Email, Password from Users", con);
            SqlDataReader reader = com.ExecuteReader();
            bool check = false;
            while (reader.Read())
            {
                if (Email.Text.Equals(reader.GetString(0)) && Password.Text.Equals(reader.GetString(1)))
                {
                    check = true;
                }
            }

            if (!check)
            {
                Error_Label.Visible = true;
            }
            else
            {
                Response.Redirect("Home.aspx");
            }

        }
    }
}