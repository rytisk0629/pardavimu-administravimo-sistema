﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;

namespace PAS
{
	public partial class ProductsCatalog : System.Web.UI.Page
	{

		SqlConnection con = new SqlConnection("Data Source=(LocalDB)\\MSSQLLocalDB;AttachDbFilename=|DataDirectory|\\PaasDB.mdf;Integrated Security=True");
		protected void Page_Load(object sender, EventArgs e)
		{
			con.Open();
			string ins = "select * from products";
			SqlCommand com = new SqlCommand(ins, con);
			com.ExecuteNonQuery();
			SqlDataAdapter sda = new SqlDataAdapter();
			sda.SelectCommand = com;
			DataSet ds = new DataSet();
			sda.Fill(ds);
			ProductsData.DataSource = ds;
			ProductsData.DataBind();
			con.Close();
		}
	}
}