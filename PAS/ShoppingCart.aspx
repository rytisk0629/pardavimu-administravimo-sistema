﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ShoppingCart.aspx.cs" Inherits="PAS.ShoppingCart" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>PAS</title>
    <link type="text/css" rel="stylesheet" href="Styles/LandingPage.css" media="screen" runat="server"/>
    <link type="text/css" rel="stylesheet" href="Styles/ShoppingCart.css" media="screen" runat="server"/>
</head>
<body>
    <form id="form1" runat="server">
        <nav>
                <label class="logo">Pardavimų administravimo sistema</label>
            <ul>
                <li><a class="active" href="#" onclick="window.open('LandingPage.aspx', '_self');">Home</a></li>
                <li><a href="#">About</a></li>
                <li><a href="#" onclick="window.open('Login.aspx', '_self');">Login</a></li>
                <li><a href="#" onclick="window.open('Registration.aspx', '_self');">Register</a></li>
                <li><a href="#" onclick="window.open('ShoppingCart.aspx', '_self');">Cart</a></li>
                <li><a href="#" onclick="window.open('MyOrders.aspx', '_self');">My Orders</a></li>
            </ul>
        </nav>
		<div class="container-table100">
			<div class="wrap-table100">
				<div class="table100">
                    <h2>Shopping cart</h2>
                    <table>
                        <thead>
                            <tr class="table100-head">
                                <th class="column1">Description</th>
                                <th class="column2">Price</th>
                                <th class="column3">Amount</th>
                            </tr>
                        </thead>
                        <asp:Repeater ID="ShoppingCartList" runat="server">
                            <ItemTemplate>   
                                    <tr>
                                        <td class="column1"><%#Eval("description") %></td>
                                        <td class="column2"><%#Eval("price") %>&euro;</td>
                                        <td class="column3"><%#Eval("amount") %></td>
                                    </tr>
                                </ItemTemplate>
                        </asp:Repeater>
                    </table>
                    <asp:Button CssClass="purchaseButton" runat="server" Text="Purchase" OnClick="btn_purchase_Click"/>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
