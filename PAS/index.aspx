﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="index.aspx.cs" Inherits="PAS.index" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link type="text/css" rel="stylesheet" href="Styles/index.css" />
</head>
<body>
    <form id="form1" runat="server">
        <div>

			<h1> Pardavimų administravimo sistema </h1>
			<br />
			<br />

			<asp:Button ID="Logout_Button" runat="server" Text="Logout" PostBackUrl="~/LandingPage.aspx"/>

        </div>
    </form>
</body>
</html>
