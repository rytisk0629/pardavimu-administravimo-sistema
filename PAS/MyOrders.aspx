﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MyOrders.aspx.cs" Inherits="PAS.MyOrders" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>PAS</title>
    <link type="text/css" rel="stylesheet" href="Styles/LandingPage.css" media="screen" runat="server"/>
    <link type="text/css" rel="stylesheet" href="Styles/MyOrders.css" media="screen" runat="server"/>
</head>
<body>
    <form id="form1" runat="server">
        <nav>
            <label class="logo">Pardavimų administravimo sistema</label>
            <ul>
                <li><a class="active" href="#" onclick="window.open('LandingPage.aspx', '_self');">Home</a></li>
                <li><a href="#">About</a></li>
                <li><a href="#" onclick="window.open('Login.aspx', '_self');">Login</a></li>
                <li><a href="#" onclick="window.open('Registration.aspx', '_self');">Register</a></li>
                <li><a href="#" onclick="window.open('ShoppingCart.aspx', '_self');">Cart</a></li>
                <li><a href="#" onclick="window.open('MyOrders.aspx', '_self');">My Orders</a></li>
            </ul>
        </nav>
		<div class="container-table100">
			<div class="wrap-table100">
				<div class="table100">
                    <h2>My Orders</h2>
                    <table>
                        <thead>
                            <tr class="table100-head">
                                <th class="column1">Description</th>
                                <th class="column2">Price</th>
                                <th class="column3">Amount</th>
                                <th class="column4">Date</th>
                            </tr>
                        </thead>
                        <asp:Repeater ID="OrdersList" runat="server">
                            <ItemTemplate>   
                                    <tr>
                                        <td class="column1"><%#Eval("description") %></td>
                                        <td class="column2"><%#Eval("price") %>&euro;</td>
                                        <td class="column3"><%#Eval("amount") %></td>
                                        <td class="column4"><%#Eval("date", "{0:d}") %></td>
                                    </tr>
                                </ItemTemplate>
                        </asp:Repeater>
                    </table>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
