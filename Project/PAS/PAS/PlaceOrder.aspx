﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PlaceOrder.aspx.cs" Inherits="PAS.PlaceOrder" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Place Order</title>
    <link href="Styles/PlaceOrder.css" rel="stylesheet" type="text/css" media="screen" runat="server" />
</head>
<body>
    <form id="form1" runat="server">
        <nav>
            <asp:Image runat="server" ImageUrl="~/Img/logo.png" CssClass="logoImg"/>
            <ul>
                <li><a href="Home.aspx">Home</a></li>
                <li><a href="ShoppingCart.aspx">Cart</a><asp:Label ID="quantLabel" runat="server"></asp:Label></li>
                <li><a href="Settings.aspx">Settings</a></li>
                <li><asp:linkbutton runat="server" ID="logout" OnClick="logout_click">LOGOUT</asp:linkbutton></li>
            </ul>
        </nav>

        <div class="all">
            <div class="information">
                <h2 class="log"> Enter your information </h2>
            <asp:TextBox runat="server" placeholder="First Name" CssClass="un" ID="firstName"></asp:TextBox>
            <asp:TextBox runat="server" placeholder="Last Name" CssClass="un" ID="lastName"></asp:TextBox>
            <br />
            <asp:TextBox runat="server" placeholder="Email" TextMode="Email" CssClass="un" ID="email"></asp:TextBox>
            <asp:TextBox runat="server" placeholder="Phone" TextMode="Phone" CssClass="un" ID="phone"></asp:TextBox>
            <br />
            <asp:TextBox runat="server" placeholder="Address" CssClass="un" ID="address"></asp:TextBox>
                <br />
                <asp:Button CssClass="purchaseButton" runat="server" Text="Purchase" OnClick="btn_purchase_Click"/>
                <br />
                <asp:Label runat="server" ID="errorLabel" CssClass="errorLabel" Visible="false"></asp:Label>
            </div>
            <div class="datalistBox">
                <h4>Cart:
                </h4>
            <asp:DataList ID="DataList1" runat="server" CssClass="productsList"  DataSourceID="SqlDataSource1">
                <ItemTemplate>
                    <asp:Label ID="productIdLabel" runat="server" Visible="false" Text='<%# Eval("productid") %>'></asp:Label>
                    <asp:Label ID="priceLabel" runat="server" Visible="false" Text='<%# Eval("price") %>'></asp:Label>
                    Name:
                    <asp:Label ID="nameLabel" runat="server" Text='<%# Eval("name") %>'></asp:Label>
                    (
                    <asp:Label ID="amountLabel" runat="server" Text='<%# Eval("amount") %>' />
                    )
                    <asp:Label ID="totalPrice" runat="server"></asp:Label>
                    <br />
                    <br />
                </ItemTemplate>
                
            </asp:DataList>
                Total:
                <asp:Label runat="server" ID="totalLabel"></asp:Label>
                <br />
                </div>
            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" SelectCommand="SELECT * FROM [ShoppingCart]"></asp:SqlDataSource>
        </div>
    </form>
</body>
</html>
