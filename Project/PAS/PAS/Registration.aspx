﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Registration.aspx.cs" Inherits="PAS.Registration" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Registration</title>
    <link type="text/css" rel="stylesheet" href="Styles/Registration.css" media="screen" runat="server" />
</head>
<body>
    <div class="main">
    <form id="form1" runat="server">
        <h2 class="reg">Please register</h2>

        <asp:TextBox ID="firstName" runat="server" CssClass="un" placeholder="First Name"></asp:TextBox>

        <asp:TextBox ID="lastName" runat="server" CssClass="un" placeholder="Last Name"></asp:TextBox>

        <asp:TextBox ID="email" runat="server" CssClass="un" placeholder="Email"></asp:TextBox>
        
        <asp:TextBox ID="password" runat="server" CssClass="un" placeholder="Password" TextMode="Password"></asp:TextBox>

        <asp:TextBox ID="phone" runat="server" CssClass="un" placeholder="Phone"></asp:TextBox>

        <asp:Button ID="Button1" runat="server" OnClick="signUpButton_Click" Text="Register" class="btn"/>

        <asp:Label Visible="false" ID="errorLabel" runat="server">Email address is already used</asp:Label>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" SelectCommand="SELECT * FROM [Users]"></asp:SqlDataSource>
    </form>
        </div>
</body>
</html>
