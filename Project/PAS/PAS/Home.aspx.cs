﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Web.Security;
using AjaxControlToolkit;
using System.IO;

namespace PAS
{
	public partial class index : System.Web.UI.Page
	{
		SqlConnection con = new SqlConnection("Data Source=(LocalDB)\\MSSQLLocalDB;AttachDbFilename=|DataDirectory|\\PaasDB.mdf;Integrated Security=True");

		protected void Page_Load(object sender, EventArgs e)
		{
			con.Open();
			string user = User.Identity.Name;
			string sql = "SELECT * FROM shoppingcart WHERE [userid] = '" + user + "'";
			SqlCommand cmd = new SqlCommand(sql, con);
			cmd.ExecuteNonQuery();
			SqlDataAdapter adapter = new SqlDataAdapter();
			adapter.SelectCommand = cmd;
			DataTable dtt = new DataTable();
			adapter.Fill(dtt);

			if (dtt != null)
			{
				quantLabel.Text = dtt.Rows.Count.ToString();
			}
			else
			{
				quantLabel.Text = "0";
			}
		}

		protected void ProductsData_ItemCommand(object source, DataListCommandEventArgs e)
		{
			if (e.CommandName == "addtocart")
			{
				TextBox qtytxtbox = (TextBox)(e.Item.FindControl("quantity"));
				string quant = qtytxtbox.Text;
				if (quant == String.Empty)
				{

				}
				string str = "insert into ShoppingCart values(@userId, @productId, @amount)";
				string ids = e.CommandArgument.ToString();
				string us = User.Identity.Name;
				//TextBox qtytxtbox = (TextBox)(e.Item.FindControl("quantity"));
				//string quant = qtytxtbox.Text;
				SqlCommand com = new SqlCommand(str, con);
				com.Parameters.AddWithValue("@userId", us);
				com.Parameters.AddWithValue("@productId", ids);
				com.Parameters.AddWithValue("@amount", quant);
				com.ExecuteNonQuery();
				con.Close();
			}

		}

		protected void logout_click(object sender, EventArgs e)
		{
			FormsAuthentication.SignOut();
			Response.Redirect("~/Login.aspx");
		}

		[System.Web.Services.WebMethod]
		[System.Web.Script.Services.ScriptMethod]
		public static Slide[] imgslides()
		{
			List<Slide> slides = new List<Slide>();
			string myimgdir = HttpContext.Current.Server.MapPath("~/Images/");

			DirectoryInfo dir = new DirectoryInfo(myimgdir);
			var myslides = from displaying in dir.GetFiles()
						   select new Slide
						   {
							   Name = displaying.Name,
							   ImagePath = "Images/" + displaying.Name
						   };
			return myslides.ToArray();
		}
	}
}