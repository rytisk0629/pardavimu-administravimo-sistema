﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;

namespace PAS
{
	public partial class PlanOrder : System.Web.UI.Page
	{
		SqlConnection conn = new SqlConnection("Data Source=(LocalDB)\\MSSQLLocalDB;AttachDbFilename=|DataDirectory|\\PaasDB.mdf;Integrated Security=True");

		protected void Page_Load(object sender, EventArgs e)
		{
			plan();
		}

		private void plan()
		{
			string days;

			switch (Time.SelectedItem.Value)
			{
				case "Week":
					days = "7";
					break;
				case "Month":
					days = "31";
					break;
				case "Year":
					days = "365";
					break;
				default:
					days = "7";
					break;
			}

			string sql = "SELECT [products].[description], [t].[sum_amount]" +
				"FROM [products]" +
				"INNER JOIN(" +
				"	SELECT [product], SUM([order].[quantity]) AS [sum_amount]" +
				"	FROM [order]" +
				"	WHERE DATEDIFF(day, [order].[orderdate], GETDATE()) <= " + days +
				"	GROUP BY [order].[product]" +
				") [t]" +
				"ON [t].[product] = [products].[id]";

			conn.Open();
			SqlCommand cmd = new SqlCommand(sql, conn);
			cmd.ExecuteNonQuery();
			SqlDataAdapter adapter = new SqlDataAdapter();
			adapter.SelectCommand = cmd;
			DataSet ds = new DataSet();
			adapter.Fill(ds);
			OrdersList.DataSource = ds;
			OrdersList.DataBind();
			conn.Close();
		}
	}
}