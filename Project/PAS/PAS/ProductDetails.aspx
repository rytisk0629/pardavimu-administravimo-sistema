﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ProductDetails.aspx.cs" Inherits="PAS.ProductDetails" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Details</title>
        <link href="Styles/ProductDetails.css" rel="stylesheet" type="text/css" media="screen" runat="server" />

</head>
<body>
    <form id="form1" runat="server">
        <nav>
            <asp:Image runat="server" ImageUrl="~/Img/logo.png" CssClass="logoImg"/>
            <ul>    
                <li><a href="Home.aspx">Home</a></li>
                <li><a href="ShoppingCart.aspx">Cart</a><asp:Label ID="quantLabel" runat="server"></asp:Label></li>
                <li><a href="Settings.aspx">Settings</a></li>
                <li><asp:linkbutton runat="server" ID="logout" OnClick="logout_click">LOGOUT</asp:linkbutton></li>
            </ul>
        </nav>


        <div id="sidebar">
            <ul>    
                <li><a href="ProductsCatalogue.aspx">All Items</a></li>

                    <li><a href="MyProducts.aspx">My Products</a></li>
                
                <li><a href="AddProduct.aspx">Add Products</a></li>
                <li><a href="MyOrders.aspx">My Orders</a></li>
                <li><a href="MySales.aspx">My Sales</a></li>
            </ul>

        </div>
        <div class="product">
            <asp:DataList ID="DataList1" runat="server" CssClass="Datalist" CellPadding="4" DataKeyField="Id" DataSourceID="SqlDataSource1" ForeColor="#333333">
                <ItemTemplate>
                    <br />
                    <asp:Image ID="Image1" runat="server" CssClass="image" imageurl='<%# "data:image/jpg;base64," + Convert.ToBase64String((byte[])Eval("Image")) %>' Height="258px" Width="278px" />
                    <div class="data" style="width: 430px">
                    <asp:Label runat="server" CssClass="name" Text="Name:" />
                    <asp:Label ID="NameLabel" runat="server" CssClass="name" Text='<%# Eval("Name") %>' />
                        <br />
                    <br />
                    <asp:Label runat="server" CssClass="name" Text="Category:" />
                    <asp:Label ID="CategoryLabel" runat="server" CssClass="category" Text='<%# Eval("Category") %>' />
                        <br />
                    <br />
                    <asp:Label runat="server" CssClass="name" Text="Brand:" />
                    <asp:Label ID="BrandLabel" runat="server" CssClass="brand" Text='<%# Eval("Brand") %>' />
                        <br />
                    <br />
                    <asp:Label runat="server" CssClass="name" Text="Price:" />
                    <asp:Label ID="PriceLabel" runat="server" CssClass="price" Text='<%# Eval("Price") %>' />
                        <br />
                    <br />
                    <asp:Label runat="server" CssClass="name" Text="Seller:" />
                    <asp:Label ID="SellerLabel" runat="server" CssClass="seller" Text='<%# Eval("Seller") %>' />
                    <br />
                    <br />
                    <asp:Label runat="server" CssClass="name" Text="Description:" />
                    <asp:Label ID="Label1" runat="server" CssClass="description" Text='<%# Eval("Description") %>' />
<br />
                     </div>
                </ItemTemplate>
                <SelectedItemStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
            </asp:DataList>
            </div>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" SelectCommand="SELECT * FROM [Products] WHERE ([Id] = @Id)">
            <SelectParameters>
                <asp:QueryStringParameter Name="Id" QueryStringField="id" Type="Int32" />
            </SelectParameters>
        </asp:SqlDataSource>
    </form>
</body>
</html>
