﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;

namespace PAS
{
    public partial class PlaceOrder : System.Web.UI.Page
    {
        SqlConnection con = new SqlConnection("Data Source=(LocalDB)\\MSSQLLocalDB;AttachDbFilename=|DataDirectory|\\PaasDB.mdf;Integrated Security=True");
        protected void Page_Load(object sender, EventArgs e)
        {
            con.Open();
            string user = User.Identity.Name;
            string sql = "SELECT [shoppingcart].[productid],[products].[name],[products].[price],[shoppingcart].[amount] FROM products INNER JOIN shoppingcart ON [shoppingcart].[productId] = [products].[Id] AND [shoppingcart].[userId] = '" + user + "'";
            SqlCommand cmd = new SqlCommand(sql, con);
            cmd.ExecuteNonQuery();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = cmd;
            DataSet ds = new DataSet();
            adapter.Fill(ds);
            DataList1.DataSourceID = null;
            DataList1.DataSource = ds;
            DataList1.DataBind();

            double sum = 0;
            foreach (DataListItem item in DataList1.Items)
            {
                if (item.ItemType == ListItemType.Item || item.ItemType == ListItemType.AlternatingItem)
                {
                    Label lbl = (Label)item.FindControl("priceLabel");
                    Label lbl2 = (Label)item.FindControl("amountLabel");
                    sum = sum + (Convert.ToDouble(lbl.Text) * Convert.ToDouble(lbl2.Text));
                }
            }
            totalLabel.Text = sum.ToString();

        }
        protected void logout_click(object sender, EventArgs e)
        {
            FormsAuthentication.SignOut();
            Response.Redirect("~/Login.aspx");
        }

        protected void btn_purchase_Click(object sender, EventArgs e)
        {

            bool nofile = false;
            bool blank = false;
            bool priceChar = false;
            char[] allowedPrice = { '.', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };
            string allowed = ".0123456789";
            bool phoneChar = false;

            foreach (char c in phone.Text)
            {
                if (!allowed.Contains(c.ToString()))
                {
                    phoneChar = true;
                    errorLabel.Text = "Phone field contains unallowed characters";
                    errorLabel.Visible = true;
                    break;
                }
            }

            if (firstName.Text == String.Empty)
            {
                blank = true;
                errorLabel.Text = "Some fields are blank";
                errorLabel.Visible = true;
            }
            if (lastName.Text == String.Empty)
            {
                blank = true;
                errorLabel.Text = "Some fields are blank";
                errorLabel.Visible = true;
            }
            if (email.Text == String.Empty)
            {
                blank = true;
                errorLabel.Text = "Some fields are blank";
                errorLabel.Visible = true;
            }
            if (phone.Text == String.Empty)
            {
                blank = true;
                errorLabel.Text = "Some fields are blank";
                errorLabel.Visible = true;
            }
            if (address.Text == String.Empty)
            {
                blank = true;
                errorLabel.Text = "Some fields are blank";
                errorLabel.Visible = true;
            }
            if (Request.Form["description"] == String.Empty)
            {
                blank = true;
                errorLabel.Text = "Some fields are blank";
                errorLabel.Visible = true;
            }

            if (blank != true && phoneChar != true)
            {
                double sum = 0;
                int quant = 0;
                foreach (DataListItem item in DataList1.Items)
                {
                    if (item.ItemType == ListItemType.Item || item.ItemType == ListItemType.AlternatingItem)
                    {
                        Label lbl = (Label)item.FindControl("priceLabel");
                        Label lbl2 = (Label)item.FindControl("amountLabel");
                        sum = sum + (Convert.ToDouble(lbl.Text) * Convert.ToInt32(lbl2.Text));
                        quant = quant + Convert.ToInt32(lbl2.Text);
                    }
                }

                string user = User.Identity.Name;
                string str = "insert into [Order] (FirstName,LastName,Email,OrderDate,Quantity,Phone,Price,Address) OUTPUT INSERTED.ID values(@FirstName, @LastName, @Email,@OrderDate,@Quantity,@Phone,@Price,@Address)";
                SqlCommand com = new SqlCommand(str, con);
                string desc = Request.Form["description"];
                com.Parameters.AddWithValue("@FirstName", firstName.Text);
                com.Parameters.AddWithValue("@LastName", lastName.Text);
                com.Parameters.AddWithValue("@Email", email.Text);
                com.Parameters.AddWithValue("@OrderDate", DateTime.Now.ToString("yyyy-MM-dd"));
                com.Parameters.AddWithValue("@Quantity", quant);
                com.Parameters.AddWithValue("@Phone", phone.Text);
                com.Parameters.AddWithValue("@Price", sum);
                com.Parameters.AddWithValue("@Address", address.Text);
                Int32 newId = (Int32)com.ExecuteScalar();
                con.Close();

                con.Open();

                foreach (DataListItem item in DataList1.Items)
                {
                    if (item.ItemType == ListItemType.Item || item.ItemType == ListItemType.AlternatingItem)
                    {
                        Label lbl = (Label)item.FindControl("productIdLabel");
                        Label lbl2 = (Label)item.FindControl("amountLabel");
                        string str2 = "insert into [OrderItem] (ProductId,OrderId,Quantity) values(@ProductId,@OrderId,@Quantity)";
                        SqlCommand com2 = new SqlCommand(str2, con);
                        com2.Parameters.AddWithValue("@ProductId", lbl.Text);
                        com2.Parameters.AddWithValue("@OrderId", newId);
                        com2.Parameters.AddWithValue("@Quantity", lbl2.Text);
                        com2.ExecuteNonQuery();

                    }
                }
                con.Close();
            }
        }
    }
}