﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PAS
{
    public partial class Login : System.Web.UI.Page
    {
        SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=|DataDirectory|PaasDB.mdf;Integrated Security=True");

        protected void Page_Load(object sender, EventArgs e)
        {
            con.Open();
        }

        protected void Login_Button_Click(object sender, EventArgs e)
        {
            SqlCommand com = new SqlCommand("select Email, Password from Users", con);

            if (AuthenticateUser(Email.Text, Password.Text))
            {
                FormsAuthentication.RedirectFromLoginPage(Email.Text,true);
                con.Close();
            }
            else
            {
                Error_Label.Visible = true;
            }
        }

        private bool AuthenticateUser(string email, string password)
        {
                SqlCommand cmd = new SqlCommand("authenticateUsers", con);
                cmd.CommandType = CommandType.StoredProcedure;


                SqlParameter paramUsername = new SqlParameter("@Email", email);
                SqlParameter paramPassword = new SqlParameter("@Password", password);

                cmd.Parameters.Add(paramUsername);
                cmd.Parameters.Add(paramPassword);

                int ReturnCode = (int)cmd.ExecuteScalar();
                return ReturnCode == 1;
        }
    }
}