﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;


namespace PAS
{
    public partial class MyProducts : System.Web.UI.Page
    {
        SqlConnection con = new SqlConnection("Data Source=(LocalDB)\\MSSQLLocalDB;AttachDbFilename=|DataDirectory|\\PaasDB.mdf;Integrated Security=True");

        protected void Page_Load(object sender, EventArgs e)
        {
            con.Open();
            SqlCommand sqlCommand = new SqlCommand();
            string user = User.Identity.Name;
            string sqlquery = "SELECT * FROM [dbo].[Products] WHERE [Seller] = '" + user + "'";
            sqlCommand.CommandText = sqlquery;
            sqlCommand.Connection = con;
            DataTable dt = new DataTable();
            SqlDataAdapter sda = new SqlDataAdapter(sqlCommand);
            SqlDataReader sdr = sqlCommand.ExecuteReader();
            if (sdr.HasRows)
            {
                ProductsData.DataSourceID = null;
                ProductsData.DataSource = sdr;
                ProductsData.DataBind();
            }
            con.Close();
        }
        protected void ProductsData_ItemCommand(object source, DataListCommandEventArgs e)
        {
            if (e.CommandName == "edit")
            {
                string id = e.CommandArgument.ToString();
                Response.Redirect("EditProduct.aspx?id=" + id);
            }

        }
        protected void logout_click(object sender, EventArgs e)
        {
            FormsAuthentication.SignOut();
            Response.Redirect("~/Login.aspx");
        }

        protected void addtocart_Command(object sender, CommandEventArgs e)
        {
            string id = e.CommandArgument.ToString();
            Response.Redirect("EditProduct.aspx?id=" + id);
        }
    }
}