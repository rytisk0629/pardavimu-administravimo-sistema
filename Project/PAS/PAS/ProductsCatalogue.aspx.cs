﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;
using System.Configuration;
using System.Data.SqlClient;
using System.Web.Security;

namespace PAS
{
	public partial class ProductsCatalogue : System.Web.UI.Page
	{
		static int currentposition = 0;
		static int totalrows = 0;
		SqlConnection con = new SqlConnection("Data Source=(LocalDB)\\MSSQLLocalDB;AttachDbFilename=|DataDirectory|\\PaasDB.mdf;Integrated Security=True");
		protected void Page_Load(object sender, EventArgs e)
		{

			if (!IsPostBack)
			{
				con.Open();

				string user = User.Identity.Name;
				string sql = "SELECT * FROM shoppingcart WHERE [userid] = '" + user + "'";
				SqlCommand cmd = new SqlCommand(sql, con);
				cmd.ExecuteNonQuery();
				SqlDataAdapter adapter = new SqlDataAdapter();
				adapter.SelectCommand = cmd;
				DataTable dtt = new DataTable();
				adapter.Fill(dtt);

				if (dtt != null)
				{
					quantLabel.Text = dtt.Rows.Count.ToString();
				}
				else
				{
					quantLabel.Text = "0";
				}

				string ins = "select * from products";
				SqlCommand com = new SqlCommand(ins, con);
				com.ExecuteNonQuery();
				SqlDataAdapter sda = new SqlDataAdapter();
				sda.SelectCommand = com;
				DataSet ds = new DataSet();
				sda.Fill(ds);

				totalrows = ds.Tables[0].Rows.Count;
				DataTable dt = ds.Tables[0];
				PagedDataSource pg = new PagedDataSource();
				pg.DataSource = dt.DefaultView;
				pg.AllowPaging = true;
				pg.CurrentPageIndex = currentposition;
				pg.PageSize = 3;
				Button1.Enabled = !pg.IsFirstPage;
				Button2.Enabled = !pg.IsFirstPage;
				Button3.Enabled = !pg.IsLastPage;
				Button4.Enabled = !pg.IsLastPage;
				if (pg.PageSize >= totalrows)
				{
					Button1.Visible = false;
					Button2.Visible = false;
					Button3.Visible = false;
					Button4.Visible = false;
				}
				//Binding pg to datalist
				ProductsData.DataSourceID = null;
				ProductsData.DataSource = pg;//dl is datalist
				ProductsData.DataBind();
				con.Close();
				//ProductsData.DataSource = ds;
				//ProductsData.DataBind();
			}
		}

		protected void logout_click(object sender, EventArgs e)
		{
			FormsAuthentication.SignOut();
			Response.Redirect("~/Login.aspx");
		}

		protected void ProductsData_ItemCommand(object source, DataListCommandEventArgs e)
		{
			if (e.CommandName == "addtocart")
			{
				con.Open();

				TextBox qtytxtbox = (TextBox)(e.Item.FindControl("quantity"));
				string quant = qtytxtbox.Text;

				bool letters = false;
				string allowed = "0123456789";
				foreach (char c in quant)
				{
					if (!allowed.Contains(c.ToString()))
					{
						letters = true;
						break;
					}
				}

				if (quant == String.Empty)
				{
					Label errorLabel = (Label)e.Item.FindControl("Label1");
					errorLabel.Visible = true;
				}
				else if(letters == true)
				{
					Label errorLabel = (Label)e.Item.FindControl("Label1");
					errorLabel.Visible = true;
				}
				else
				{
					string str = "insert into ShoppingCart values(@userId, @productId, @amount)";
					string ids = e.CommandArgument.ToString();
					string us = User.Identity.Name;
					SqlCommand com = new SqlCommand(str, con);
					com.Parameters.AddWithValue("@userId", us);
					com.Parameters.AddWithValue("@productId", ids);
					com.Parameters.AddWithValue("@amount", quant);
					com.ExecuteNonQuery();

					string user = User.Identity.Name;
					string sql = "SELECT * FROM shoppingcart WHERE [userid] = '" + user + "'";
					SqlCommand cmd = new SqlCommand(sql, con);
					cmd.ExecuteNonQuery();
					SqlDataAdapter adapter = new SqlDataAdapter();
					adapter.SelectCommand = cmd;
					DataTable dtt = new DataTable();
					adapter.Fill(dtt);

					if (dtt != null)
					{
						quantLabel.Text = dtt.Rows.Count.ToString();
					}
					else
					{
						quantLabel.Text = "0";
					}
					con.Close();

					Label errorLabel = (Label)e.Item.FindControl("Label1");
					if(errorLabel.Visible == true)
					{
						errorLabel.Visible = false;
					}
				}
			}

			if (e.CommandName == "viewdetail")
			{
				Response.Redirect("ProductDetails.aspx?id=" + e.CommandArgument.ToString());
			}
		}

		protected void btn_search_Click(object sender, EventArgs e)
		{
            con.Open();
            SqlCommand sqlCommand = new SqlCommand();
            string sqlquery = "SELECT * FROM [dbo].[Products] WHERE name LIKE '%'+@name+'%'";
            sqlCommand.CommandText = sqlquery;
            sqlCommand.Connection = con;
            sqlCommand.Parameters.AddWithValue("name", txt_search.Text.ToString());
            DataTable dt = new DataTable();
            SqlDataAdapter sda = new SqlDataAdapter(sqlCommand);
			SqlDataReader sdr = sqlCommand.ExecuteReader();
			if (sdr.HasRows)
			{
				ProductsData.DataSourceID = null;
				ProductsData.DataSource = sdr;
				ProductsData.DataBind();
			}
			con.Close();
        }
		private void bindata() 
		{
			con.Open();
			string ins = "select * from products";
			SqlCommand com = new SqlCommand(ins, con);
			com.ExecuteNonQuery();
			SqlDataAdapter sda = new SqlDataAdapter();
			sda.SelectCommand = com;
			DataSet ds = new DataSet();
			sda.Fill(ds);

			totalrows = ds.Tables[0].Rows.Count;
			DataTable dt = ds.Tables[0];
			PagedDataSource pg = new PagedDataSource();
			pg.DataSource = dt.DefaultView;
			pg.AllowPaging = true;
			pg.CurrentPageIndex = currentposition;
			pg.PageSize = 3;
			Button1.Enabled = !pg.IsFirstPage;
			Button2.Enabled = !pg.IsFirstPage;
			Button3.Enabled = !pg.IsLastPage;
			Button4.Enabled = !pg.IsLastPage;
			if(pg.PageSize >= totalrows)
			{
				Button1.Visible = false;
				Button2.Visible = false;
				Button3.Visible = false;
				Button4.Visible = false;
			}
			//Binding pg to datalist
			ProductsData.DataSourceID = null;
			ProductsData.DataSource = pg;//dl is datalist
			ProductsData.DataBind();
			con.Close();
			//ProductsData.DataSource = ds;
			//ProductsData.DataBind();
		}
		protected void Button1_Click(object sender, EventArgs e)
		{
			currentposition = 0;
			bindata();

		}

		protected void Button2_Click(object sender, EventArgs e)
		{
			if (currentposition == 0)
			{

			}
			else
			{
				currentposition = currentposition - 1;
				bindata();
			}
		}

		protected void Button3_Click(object sender, EventArgs e)
		{
			if (currentposition == totalrows - 1)
			{

			}
			else
			{
				currentposition = currentposition + 1;
				bindata();
			}

		}

		protected void Button4_Click(object sender, EventArgs e)
		{
			currentposition = totalrows - 1;
			bindata();
		}

		protected void filterButton_Click(object sender, EventArgs e)
		{
			con.Open();
			SqlCommand sqlCommand = new SqlCommand();
			int cat = Convert.ToInt32(categoryList.SelectedValue);
			string sqlquery = "SELECT * FROM [dbo].[Products] WHERE Category=@Cat";
			sqlCommand.CommandText = sqlquery;
			sqlCommand.Connection = con;
			sqlCommand.Parameters.AddWithValue("@Cat", cat);
			DataTable dt = new DataTable();
			SqlDataAdapter sda = new SqlDataAdapter(sqlCommand);
			SqlDataReader sdr = sqlCommand.ExecuteReader();

				ProductsData.DataSourceID = null;
				ProductsData.DataSource = sdr;
				ProductsData.DataBind();
			
			con.Close();
		}
	}
}