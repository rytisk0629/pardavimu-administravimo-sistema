﻿﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using System.Web.Security;

namespace PAS
{
	public partial class ShoppingCart : System.Web.UI.Page
	{
		SqlConnection conn = new SqlConnection("Data Source=(LocalDB)\\MSSQLLocalDB;AttachDbFilename=|DataDirectory|\\PaasDB.mdf;Integrated Security=True");
		protected void Page_Load(object sender, EventArgs e)
		{
			string user = User.Identity.Name;
			conn.Open();
			string sql = "SELECT [products].[Id],[products].[name],[products].[price],[shoppingcart].[productId],[shoppingcart].[amount]" +
				" FROM products INNER JOIN shoppingcart ON [shoppingcart].[productId] = [products].[Id] AND [shoppingcart].[userId] = '" + user + "'";
			SqlCommand cmd = new SqlCommand(sql, conn);
			cmd.ExecuteNonQuery();
			SqlDataAdapter adapter = new SqlDataAdapter();
			adapter.SelectCommand = cmd;
			DataSet ds = new DataSet();
			adapter.Fill(ds);
			ShoppingCartList.DataSource = ds;
			ShoppingCartList.DataBind();
			conn.Close();
		}

		protected void DeleteRow(object sender, EventArgs e)
		{
			LinkButton btn = (LinkButton)sender;
			string productId = btn.CommandArgument;
			string user = User.Identity.Name;
			SqlCommand cmd = new SqlCommand("DELETE FROM ShoppingCart Where ProductId = @ProductId AND UserId = @UserId", conn);
			cmd.Parameters.AddWithValue("@ProductId", productId);
			cmd.Parameters.AddWithValue("@UserId", user);
			conn.Open();
			cmd.ExecuteNonQuery();
			conn.Close();
			Response.Redirect("ShoppingCart.aspx");
		}
		protected void logout_click(object sender, EventArgs e)
		{
			FormsAuthentication.SignOut();
			Response.Redirect("~/Login.aspx");
		}
		protected void btn_purchase_Click(object sender, EventArgs e)
		{
			{
				//conn.Open();
				//string user = User.Identity.Name;
				//string sql = "SELECT * FROM shoppingcart WHERE [user] = '" + user + "'";
				//SqlCommand cmd = new SqlCommand(sql, conn);
				//cmd.ExecuteNonQuery();
				//SqlDataAdapter adapter = new SqlDataAdapter();
				//adapter.SelectCommand = cmd;
				//DataSet ds = new DataSet();
				//adapter.Fill(ds);

				//sql = "INSERT INTO orders ([user], [product], [amount], [date]) VALUES ";
				//string date = DateTime.Now.ToString("MM/dd/yyyy");
				//foreach (DataRow row in ds.Tables[0].Rows)
				//{
				//	sql += "('" + row["user"].ToString().Trim() + "', '" + row["productId"].ToString().Trim() + "', '" + row["amount"].ToString().Trim() + "', '" + date + "'), ";
				//}
				//sql = sql.Substring(0, sql.Length - 2);

				//cmd = new SqlCommand(sql, conn);
				//cmd.ExecuteNonQuery();


				//sql = "DELETE FROM shoppingcart WHERE [user] = '" + user + "'";
				//cmd = new SqlCommand(sql, conn);
				//cmd.ExecuteNonQuery();

				//conn.Close();

				Response.Redirect("~/PlaceOrder.aspx");
			}
		}
	}
}