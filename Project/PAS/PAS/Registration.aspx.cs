﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

namespace PAS
{
    public partial class Registration : System.Web.UI.Page
    {
        SqlConnection con = new SqlConnection("Data Source=(LocalDB)\\MSSQLLocalDB;AttachDbFilename=|DataDirectory|\\PaasDB.mdf;Integrated Security=True;MultipleActiveResultSets=true");
        protected void Page_Load(object sender, EventArgs e)
        {
            con.Open();
        }

        protected void signUpButton_Click(object sender, EventArgs e)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "select * from [Users]";
            cmd.Connection = con;
            SqlDataReader rd = cmd.ExecuteReader();
            bool flag = false;
            bool blank = false;
            bool phoneChar = false;
            char[] allowedPhone = { '+', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };
            string allowed = "+0123456789";
            foreach (char c in phone.Text)
            {
                if (!allowed.Contains(c.ToString()))
                {
                    phoneChar = true;
                    errorLabel.Text = "Phone field contains unallowed characters";
                    errorLabel.Visible = true;
                    break;
                }
            }

            if (firstName.Text == String.Empty)
            {
                blank = true;
                errorLabel.Text = "Some fields are blank";
                errorLabel.Visible = true;
            }
            if (lastName.Text == String.Empty)
            {
                blank = true;
                errorLabel.Text = "Some fields are blank";
                errorLabel.Visible = true;
            }
            if (email.Text == String.Empty)
            {
                blank = true;
                errorLabel.Text = "Some fields are blank";
                errorLabel.Visible = true;
            }
            if (password.Text == String.Empty)
            {
                blank = true;
                errorLabel.Text = "Some fields are blank";
                errorLabel.Visible = true;
            }
            if (phone.Text == String.Empty)
            {
                blank = true;
                errorLabel.Text = "Some fields are blank";
                errorLabel.Visible = true;
            }

            if (blank != true && phoneChar != true)
            {
                while (rd.Read())
                {
                    if (rd[3].ToString() == Request.Form["email"])
                    {
                        flag = true;
                        break;
                    }
                }
                if (flag == true)
                {
                    errorLabel.Text = "This email address is already used";
                    errorLabel.Visible = true;
                }
                else
                {
                    string ins = "INSERT INTO Users (FirstName,LastName,Email,Password,Phone) VALUES('" + Request.Form["firstName"] + "', '" + Request.Form["lastName"] + "', '" + Request.Form["email"] + "', '" + Request.Form["password"] + "', '" + Request.Form["phone"] + "')";
                    SqlCommand com = new SqlCommand(ins, con);
                    com.ExecuteNonQuery();
                    con.Close();
                    Response.Redirect("Login.aspx");
                }
            }
        }

        protected void SqlDataSource1_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
        {

        }
    }
}