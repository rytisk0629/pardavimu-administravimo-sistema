﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EditProduct.aspx.cs" Inherits="PAS.EditProduct" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Edit</title>
        <link href="Styles/AddProduct.css" rel="stylesheet" type="text/css" media="screen" runat="server" />

</head>
<body>
    <form id="form1" runat="server">
        <nav>
            <asp:Image runat="server" ImageUrl="~/Img/logo.png" CssClass="logoImg"/>
            <ul>
                <li><a href="Home.aspx">Home</a></li>
                <li><a href="ShoppingCart.aspx">Cart</a><asp:Label ID="quantLabel" runat="server"></asp:Label></li>
                <li><a href="Settings.aspx">Settings</a></li>
                <li><asp:linkbutton runat="server" ID="logout" OnClick="logout_click">LOGOUT</asp:linkbutton></li>
            </ul>
        </nav>


        <div id="sidebar">
            <ul>    
                <li><a href="ProductsCatalogue.aspx">All Items</a></li>
                
                    <li><a href="MyProducts.aspx">My Products</a></li>
                
                <li><a href="AddProduct.aspx">Add Products</a></li>
                <li><a href="MyOrders.aspx">My Orders</a></li>
                <li><a href="MySales.aspx">My Sales</a></li>
            </ul>

        </div>

        <h1 class="addMessage">Edit product</h1>

        <div class="add">
        <asp:TextBox ID="name" runat="server" placeholder="Name" CssClass="un"></asp:TextBox>

        <asp:DropDownList runat="server" ID="categoryList" DataSourceID="SqlDataSource2" DataTextField="Name" DataValueField="Id" CssClass="un"></asp:DropDownList>

            <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" SelectCommand="SELECT * FROM [Category]"></asp:SqlDataSource>

        <asp:TextBox ID="brand" runat="server" placeholder="Brand" CssClass="un"></asp:TextBox>

        <asp:TextBox ID="price" runat="server" placeholder="Price" CssClass="un"></asp:TextBox>

        <asp:TextBox ID="description" runat="server" CssClass="un" placeholder="Product description" TextMode="MultiLine"></asp:TextBox>

        <asp:FileUpload ID="fileImage" runat="server" CssClass="upload" />
            
        <asp:Button ID="submit" runat="server" Text="Update" OnClick="submit_Click1" class="sumbmit" />
            <asp:Label ID="errorLabel" Visible="false" runat="server" CssClass="errorLabel"></asp:Label>
        </div>
    </form>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" SelectCommand="SELECT * FROM [Products]"></asp:SqlDataSource>
</body>
</html>
