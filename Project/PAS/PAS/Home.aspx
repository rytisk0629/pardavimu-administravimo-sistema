﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Home.aspx.cs" Inherits="PAS.index" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Home</title>
    <link href="Styles/Home.css" rel="stylesheet" type="text/css" media="screen" runat="server" />
    <script type="text/javascript" src="~/js/Slides.js"></script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <nav>
            <asp:Image runat="server" ImageUrl="~/Img/logo.png" CssClass="logoImg"/>
            <ul>
                <li><a href="Home.aspx">Home</a></li>
                <li><a href="ShoppingCart.aspx">Cart</a><asp:Label ID="quantLabel" runat="server"></asp:Label></li>
                <li><a href="Settings.aspx">Settings</a></li>
                <li><asp:linkbutton runat="server" ID="logout" OnClick="logout_click">LOGOUT</asp:linkbutton></li>
            </ul>
        </nav>
        
        <div id="sidebar">
            <ul>    
                <li><a href="ProductsCatalogue.aspx">All Items</a></li>
                
                    <li><a href="MyProducts.aspx">My Products</a></li>
                
                <li><a href="AddProduct.aspx">Add Products</a></li>
                <li><a href="MyOrders.aspx">My Orders</a></li>
                <li><a href="MySales.aspx">My Sales</a></li>
                <li><a href="GenerateReport.aspx">Report</a></li>
            </ul>

        </div>

        <div class="offerImage">
        <asp:Image ID="Image1" runat="server" Height="320px" Width="550px"  />
        <ajaxToolKit:SlideShowExtender ID="SlideShowExtender1" runat="server"  AutoPlay="true" PlayInterval="3000" Loop="true"
             SlideShowAnimationType="SlideRight" TargetControlID="Image1" SlideShowServiceMethod="imgslides" />
        </div>
        <div class="newest">
            <h1>Newest items</h1>
         </div>

         <div class="products">
                <asp:DataList ID="ProductsData" runat="server" RepeatColumns="4" DataSourceID="SqlDataSource1" OnItemCommand="ProductsData_ItemCommand">
                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" />
                <ItemTemplate>
                    <div class="product">
                        <div class="image">
                            <asp:Image ID="Image1" runat="server" Height="100px" ImageUrl='<%# "data:image/jpg;base64," + Convert.ToBase64String((byte[])Eval("Image")) %>' Width="100px" />
                        </div>
                        <div class="description"><%#Eval("Name") %></div>

                        <div class="price"><%#Eval("Price")%>&euro;
                            <div>
                            <asp:Label class="quantLabel" runat="server"  Text="Quantity:"></asp:Label>
                            <asp:TextBox class="quantBox" ID="quantity" runat="server" Text="1"></asp:TextBox>
                            </div>
                        </div>
                        <asp:Button class="purchaseButton" ID="addtocart" runat="server" Text="Add To Cart" CommandArgument='<%#Eval("Id") %>' CommandName="addtocart" />
                        <br />
                        <br />
                    </div>
                </ItemTemplate>

			    </asp:DataList>
          </div>
                <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" SelectCommand="SELECT * FROM [Products]"></asp:SqlDataSource>
       
            
    </form>

</body>
</html>
