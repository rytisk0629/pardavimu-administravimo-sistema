﻿	<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="PAS.Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Login</title>
	<link type="text/css" rel="stylesheet" href="~/Styles/Login.css" runat="server" />
</head>
<body>
    <div class="main">
        <form id="Form1" runat="server">
            <div>

			    <h2 class="log"> Login </h2>
			    <asp:TextBox class="un" ID="Email" runat="server" style="margin-top: 0px" placeholder="Email"></asp:TextBox>
			    <br />
			    <asp:TextBox class="un" ID="Password" runat="server" TextMode="Password" placeholder="Password"></asp:TextBox>
			    <br />
			    <br />
			    <asp:Button class="submit" ID="Login_Button" runat="server" Text="Login" OnClick="Login_Button_Click"/>
			    <br />
			    <br />
			    <asp:Label class="error" ID="Error_Label" runat="server" Text="Incorect email adress or password" ForeColor="Red" Visible="False"></asp:Label>
            </div>
        </form>
    </div>
</body>
</html>
