﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ShoppingCart.aspx.cs" Inherits="PAS.ShoppingCart" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Cart</title>
    <link type="text/css" rel="stylesheet" href="Styles/LandingPage.css"/>
    <link type="text/css" rel="stylesheet" href="Styles/ShoppingCart.css"/>
</head>
<body>
    <form id="form1" runat="server">
        <nav>
            <asp:Image runat="server" ImageUrl="~/Img/logo.png" CssClass="logoImg"/>
            <ul>
                <li><a href="Home.aspx">Home</a></li>
                <li><a href="ShoppingCart.aspx">Cart</a><asp:Label ID="quantLabel" runat="server"></asp:Label></li>
                <li><a href="Settings.aspx">Settings</a></li>
                <li><asp:linkbutton runat="server" ID="logout" OnClick="logout_click">LOGOUT</asp:linkbutton></li>
            </ul>
        </nav>
		<div class="container-table100">
			<div class="wrap-table100">
				<div class="table100">
                    <h2>Shopping cart</h2>
                    <table>
                        <thead>
                            <tr class="table100-head">
                                <th class="column1">Name</th>
                                <th class="column2">Price</th>
                                <th class="column3">Amount</th>
                                <th class="column1">Remove</th>
                            </tr>
                        </thead>
                        <asp:Repeater ID="ShoppingCartList" runat="server">
                            <ItemTemplate>   
                                    <tr>
                                        <td class="column1"><%#Eval("name") %></td>
                                        <td class="column2"><%#Eval("price") %>&euro;</td>
                                        <td class="column3"><%#Eval("amount") %></td>
                                        <td class="column1"><asp:LinkButton runat="server" ID="labelRemove" OnClick="DeleteRow" CommandName="DeleteRow" CommandArgument='<%#Eval("ProductId")%>'>Remove</asp:LinkButton></td>
                                    </tr>
                                </ItemTemplate>
                        </asp:Repeater>
                    </table>
                    <asp:Button CssClass="purchaseButton" runat="server" Text="Purchase" OnClick="btn_purchase_Click"/>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
