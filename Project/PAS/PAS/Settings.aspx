﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Settings.aspx.cs" Inherits="PAS.Settings" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Settings</title>
    <link href="Styles/Settings.css" rel="stylesheet" type="text/css" media="screen" runat="server" />
</head>
<body>
    <form id="form1" runat="server">
        <nav>
            <asp:Image runat="server" ImageUrl="~/Img/logo.png" CssClass="logoImg"/>
            <ul>
                <li><a href="Home.aspx">Home</a></li>
                <li><a href="ShoppingCart.aspx">Cart</a><asp:Label ID="quantLabel" runat="server"></asp:Label></li>
                <li><a href="Settings.aspx">Settings</a></li>
                <li><asp:linkbutton runat="server" ID="logout" OnClick="logout_click">LOGOUT</asp:linkbutton></li>
            </ul>
        </nav>
        
        <div id="sidebar">
            <ul>    
                <li><a href="ProductsCatalogue.aspx">All Items</a></li>
                
                    <li><a href="MyProducts.aspx">My Products</a></li>
                
                <li><a href="AddProduct.aspx">Add Products</a></li>
                <li><a href="MyOrders.aspx">My Orders</a></li>
                <li><a href="MySales.aspx">My Sales</a></li>
            </ul>

        </div>
        <div class="changePassword">
            <h3 class="changeHeader">Change Password</h3>
            <br />
            <asp:TextBox runat="server" ID="currentPassword" placeholder="Current Password" CssClass="un" TextMode="Password"></asp:TextBox>
            <br />
            <asp:TextBox runat="server" ID="newPassword" placeholder="New Password" CssClass="un" TextMode="Password"></asp:TextBox>
            <br />
            <asp:TextBox runat="server" ID="repeatNew" placeholder="Confirm Password" CssClass="un" TextMode="Password"></asp:TextBox>
            <br />
            <asp:Button runat="server" Text="Submit" CssClass="submitButton" OnClick="Unnamed1_Click"/>
            <br />
            <asp:Label runat="server" ID="errorLabel" Visible="false" Text="ERROR" CssClass="errorLabel"></asp:Label>
            <asp:Label runat="server" ID="successLabel" Visible="false" Text="SUCCESS" CssClass="successLabel"></asp:Label>
        </div>
    </form>
</body>
</html>
