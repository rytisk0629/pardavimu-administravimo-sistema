﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MySales.aspx.cs" Inherits="PAS.MySales" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>My Sales</title>
        <link type="text/css" rel="stylesheet" href="Styles/LandingPage.css" media="screen" runat="server"/>
    <link type="text/css" rel="stylesheet" href="Styles/MyOrders.css" media="screen" runat="server"/>
</head>
<body>
    <form id="form1" runat="server">
			
		<nav>
            <asp:Image runat="server" ImageUrl="~/Img/logo.png" CssClass="logoImg"/>
            <ul>    
                <li><a href="Home.aspx">Home</a></li>
                <li><a href="ShoppingCart.aspx">Cart</a><asp:Label ID="quantLabel" runat="server"></asp:Label></li>
                <li><a href="Settings.aspx">Settings</a></li>
                <li><asp:linkbutton runat="server" ID="logout" OnClick="logout_click">LOGOUT</asp:linkbutton></li>
            </ul>
        </nav>
        <div id="sidebar">
            <ul>    
                <li><a href="ProductsCatalogue.aspx">All Items</a></li>
                
                    <li><a href="MyProducts.aspx">My Products</a></li>
                
                <li><a href="AddProduct.aspx">Add Products</a></li>
                <li><a href="MyOrders.aspx">My Orders</a></li>
                <li><a href="MySales.aspx">My Sales</a></li>
            </ul>

        </div>

		<div class="container-table100">
			<div class="wrap-table100">
				<div class="table100">
                    <h2>My Sales</h2>
                    <table>
                        <thead>
                            <tr class="table100-head">
                                <th class="column1">ID</th>
                                <th class="column2">Name</th>
                                <th class="column2">Amount</th>
                                <th class="column3">Date</th>
                                <th class="column4">Address</th>
                                <th class="column1"></th>
                            </tr>
                        </thead>
                        <asp:Repeater ID="SalesList" runat="server">
                            <ItemTemplate>   
                                    <tr>
                                        <td class="column1"><%#Eval("OrderId") %></td>
                                        <td class="column2"><%#Eval("Name") %></td>
                                        <td class="column2"><%#Eval("Quantity") %></td>
                                        <td class="column3"><%#Eval("OrderDate", "{0:d}") %></td>
                                        <td class="column4"><%#Eval("Address") %></td>
                                        <td class="column1"><asp:DataList runat="server" ></asp:DataList></td>
                                    </tr>
                                </ItemTemplate>
                        </asp:Repeater>
                    </table>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
