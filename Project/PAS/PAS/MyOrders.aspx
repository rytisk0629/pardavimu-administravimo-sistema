﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MyOrders.aspx.cs" Inherits="PAS.MyOrders" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>My Orders</title>
    <link type="text/css" rel="stylesheet" href="Styles/LandingPage.css" media="screen" runat="server"/>
    <link type="text/css" rel="stylesheet" href="Styles/MyOrders.css" media="screen" runat="server"/>
</head>
<body>
    <form id="form1" runat="server">
        <nav>
            <asp:Image runat="server" ImageUrl="~/Img/logo.png" CssClass="logoImg"/>
            <ul>    
                <li><a href="Home.aspx">Home</a></li>
                <li><a href="ShoppingCart.aspx">Cart</a><asp:Label ID="quantLabel" runat="server"></asp:Label></li>
                <li><a href="Settings.aspx">Settings</a></li>
                <li><asp:linkbutton runat="server" ID="logout" OnClick="logout_click">LOGOUT</asp:linkbutton></li>
            </ul>
        </nav>
        <div id="sidebar">
            <ul>    
                <li><a href="ProductsCatalogue.aspx">All Items</a></li>

                    <li><a href="MyProducts.aspx">My Products</a></li>
                
                <li><a href="AddProduct.aspx">Add Products</a></li>
                <li><a href="MyOrders.aspx">My Orders</a></li>
                <li><a href="MySales.aspx">My Sales</a></li>
            </ul>

        </div>

		<div class="container-table100">
			<div class="wrap-table100">
				<div class="table100">
                    <h2>My Orders</h2>
                    <table>
                        <thead>
                            <tr class="table100-head">
                                <th class="column1">ID</th>
                                <th class="column2">Date</th>
                                <th class="column3">Amount</th>
                                <th class="column4">Price</th>
                                <th class="column1"></th>
                            </tr>
                        </thead>
                        <asp:Repeater ID="OrdersList" runat="server">
                            <ItemTemplate>   
                                    <tr>
                                        <td class="column1"><%#Eval("Id") %></td>
                                        <td class="column2"><%#Eval("OrderDate", "{0:d}") %></td>
                                        <td class="column3"><%#Eval("Quantity") %></td>
                                        <td class="column4"><%#Eval("Price") %>&euro;</td>
                                        <td class="column1"><asp:LinkButton runat="server" ID="detailsButton" CommandArgument='<%#Eval("Id") %>' OnClick="viewDetails_Click" Text="Details"></asp:LinkButton></td>
                                    </tr>
                                </ItemTemplate>
                        </asp:Repeater>
                    </table>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
