﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Web.Security;

namespace PAS
{
	public partial class GenerateReport : System.Web.UI.Page
	{
		SqlConnection con = new SqlConnection("Data Source=(LocalDB)\\MSSQLLocalDB;AttachDbFilename=|DataDirectory|\\PaasDB.mdf;Integrated Security=True");
		protected void Page_Load(object sender, EventArgs e)
		{
			con.Open();
			string sql = "select Concat([Order].FirstName, ' ', [Order].LastName) as Name, [Order].Email, [Order].OrderDate, " +
							"[Order].Quantity, Products.Name, Products.Brand, Products.Price from Products " +
							"inner join OrderItem on Products.Id = OrderItem.ProductId  inner join [Order] on " +
							"[Order].id = OrderItem.OrderId" +
							"ORDER BY [Order].OrderDate ASC";

			string sql1 = "SELECT [products].[description], [t].[sum_amount]" +
				"FROM [products]" +
				"INNER JOIN(" +
				"	SELECT [product], SUM([order].[quantity]) AS [sum_amount]" +
				"	FROM [order]" +
				"	WHERE DATEDIFF(day, [order].[orderdate], GETDATE()) <= " + days +
				"	GROUP BY [order].[product]" +
				") [t]" +
				"ON [t].[product] = [products].[id]";

			SqlCommand com = new SqlCommand(sql, con);
			com.ExecuteNonQuery();
			SqlDataAdapter sda = new SqlDataAdapter();
			sda.SelectCommand = com;
			DataSet ds = new DataSet();
			sda.Fill(ds);
			Data.DataSource = ds;
			Data.DataBind();
			con.Close();
		}
		protected void logout_click(object sender, EventArgs e)
		{
			FormsAuthentication.SignOut();
			Response.Redirect("~/Login.aspx");
		}
	}
}