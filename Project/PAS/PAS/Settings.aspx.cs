﻿using Microsoft.SqlServer.Server;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PAS
{
    public partial class Settings : System.Web.UI.Page
    {
        SqlConnection con = new SqlConnection("Data Source=(LocalDB)\\MSSQLLocalDB;AttachDbFilename=|DataDirectory|\\PaasDB.mdf;Integrated Security=True");

        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void logout_click(object sender, EventArgs e)
        {
            FormsAuthentication.SignOut();
            Response.Redirect("~/Login.aspx");
        }

        protected void Unnamed1_Click(object sender, EventArgs e)
        {
            string user = User.Identity.Name;
            con.Open();
            string sql = "SELECT Password FROM Users WHERE Email = '" + user + "'";
            SqlCommand cmdd = new SqlCommand(sql, con);
            cmdd.CommandType = CommandType.Text;
            SqlDataReader sdr = cmdd.ExecuteReader();
            sdr.Read();
            string password = sdr["Password"].ToString();
            sdr.Close();
            cmdd.Dispose();
            con.Close();
            if(currentPassword.Text == password)
            {
                con.Open();
                string str = "update Users set Password=@Password where Email=@Email";
                SqlCommand com = new SqlCommand(str, con);
                com.Parameters.AddWithValue("@Password", newPassword.Text);
                com.Parameters.AddWithValue("@Email", user);
                com.ExecuteNonQuery();
                con.Close();
            }
            else
            {
                errorLabel.Visible = true;
            }
        }
    }
}