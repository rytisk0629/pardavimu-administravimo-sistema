﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Web.Security;

namespace PAS
{
	public partial class MyOrders : System.Web.UI.Page
	{
		SqlConnection conn = new SqlConnection("Data Source=(LocalDB)\\MSSQLLocalDB;AttachDbFilename=|DataDirectory|\\PaasDB.mdf;Integrated Security=True");
		

		protected void Page_Load(object sender, EventArgs e)
		{
			string user = User.Identity.Name;
			conn.Open();
			string sql = "SELECT * FROM [order] WHERE [order].[email] = '" + user + "'";
			SqlCommand cmd = new SqlCommand(sql, conn);
			cmd.ExecuteNonQuery();
			SqlDataAdapter adapter = new SqlDataAdapter();
			adapter.SelectCommand = cmd;
			DataSet ds = new DataSet();
			adapter.Fill(ds);
			OrdersList.DataSource = ds;
			OrdersList.DataBind();
			conn.Close();
		}
		protected void logout_click(object sender, EventArgs e)
		{
			FormsAuthentication.SignOut();
			Response.Redirect("~/Login.aspx");
		}
		
		protected void viewDetails_Click(object sender, EventArgs e)
		{
			string id = (sender as LinkButton).CommandArgument;
			Response.Redirect("OrderDetails.aspx?id=" + id);
		}
	}
}