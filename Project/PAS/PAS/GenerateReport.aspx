﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="GenerateReport.aspx.cs" Inherits="PAS.GenerateReport" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
	<link href="Styles/GenerateReport.css" rel="stylesheet" type="text/css" media="screen" runat="server" />
</head>
<body>
    <form id="form1" runat="server">
		<nav>
            <asp:Image runat="server" ImageUrl="~/Img/logo.png" CssClass="logoImg"/>
            <ul>
                <li><a href="Home.aspx">Home</a></li>
                <li><a href="ShoppingCart.aspx">Cart</a><asp:Label ID="quantLabel" runat="server"></asp:Label></li>
                <li><a href="Settings.aspx">Settings</a></li>
                <li><asp:linkbutton runat="server" ID="logout" OnClick="logout_click">LOGOUT</asp:linkbutton></li>
            </ul>
        </nav>
        
        <div id="sidebar">
            <ul>    
                <li><a href="ProductsCatalogue.aspx">All Items</a></li>
                
                    <li><a href="MyProducts.aspx">My Products</a></li>
                
                <li><a href="AddProduct.aspx">Add Products</a></li>
                <li><a href="MyOrders.aspx">My Orders</a></li>
                <li><a href="MySales.aspx">My Sales</a></li>
            </ul>

        </div>

		<button class="btn btn-info" id="Show-report">Generate report of sales</button>
        <div class ="report hide" id ="Report">
			<h1>Report of sales</h1>
			<table class="table">
					<thead class="thead-dark">
						<tr>
							<th scope="col">Name</th>
							<th scope="col">Email</th>
							<th scope="col">Order Date</th>
							<th scope="col">Quantity</th>
							<th scope="col">Product Name</th>
							<th scope="col">Product Brand</th>
							<th scope="col">Product Price</th>
						</tr>
					</thead>
					<tbody>
						<asp:ListView ID="Data" runat="server" RepeatColumns="1">
					
							<ItemTemplate>
									<tr>
										<td><%#Eval("Name") %></td>
										<td><%#Eval("Email") %></td>
										<td><%#Eval("OrderDate") %></td>
										<td><%#Eval("Quantity") %></td>
										<td><%#Eval("Name") %></td>
										<td><%#Eval("Brand") %></td>
										<td><%#Eval("Price") %></td>
									</tr>
							</ItemTemplate>

						</asp:ListView>

					</tbody>

					
				</table>

			<button class="btn btn-danger" id="Hide-report">exit</button>
        </div>
		<script>
			function btn1Click(e) {
				e.preventDefault();
				document.querySelector("#Show-report").classList.add("hide");
				document.querySelector("#Report").classList.remove("hide");
			}

			function btn2Click(e) {
				e.preventDefault();
				document.querySelector("#Report").classList.add("hide");
				document.querySelector("#Show-report").classList.remove("hide");
			}

			var btn1 = document.querySelector("#Show-report");
			var btn2 = document.querySelector("#Hide-report");
			btn1.onclick = btn1Click;
			btn2.onclick = btn2Click;
		</script>
    </form>


</body>
</html>
