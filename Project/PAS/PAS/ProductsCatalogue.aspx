﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ProductsCatalogue.aspx.cs" Inherits="PAS.ProductsCatalogue" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Catalog</title>
    <link href="Styles/ProductsCatalog.css" rel="stylesheet" type="text/css" media="screen" runat="server" />
    <script type="text/javascript" src="js/Sidebar.js"></script>
</head>
<body>
    <form id="form1" runat="server">

        <nav>
            <asp:Image runat="server" ImageUrl="~/Img/logo.png" CssClass="logoImg"/>
            <ul>
                <li><a href="Home.aspx">Home</a></li>
                <li><a href="ShoppingCart.aspx">Cart</a><asp:Label ID="quantLabel" runat="server"></asp:Label></li>
                <li><a href="Settings.aspx">Settings</a></li>
                <li><asp:linkbutton runat="server" ID="logout" OnClick="logout_click">LOGOUT</asp:linkbutton></li>
            </ul>
        </nav>

        <div class="search-container">
                <asp:TextBox ID="txt_search" CssClass="search-text" runat="server"></asp:TextBox>
                <asp:Button ID="btn_search" class="search-button" name="search" runat="server" Text="Search" OnClick="btn_search_Click"/>
                <asp:DropDownList ID="categoryList" runat="server" DataSourceID="SqlDataSource2" DataTextField="Name" DataValueField="Id" CssClass="search-text"></asp:DropDownList>
            <asp:Button ID="filterButton" runat="server" Text="Filter" OnClick="filterButton_Click" CssClass="filterButton"/>
                <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" SelectCommand="SELECT * FROM [Category]"></asp:SqlDataSource>
        </div>

        <div id="sidebar">
            <ul>    
                <li><a href="ProductsCatalogue.aspx">All Items</a></li>
                    <li><a href="MyProducts.aspx">My Products</a></li>
                <li><a href="AddProduct.aspx">Add Products</a></li>
                <li><a href="MyOrders.aspx">My Orders</a></li>
                <li><a href="MySales.aspx">My Sales</a></li>
            </ul>

        </div>

        <h1 class="all"></h1>
                
        <div class="products">
                <asp:DataList ID="ProductsData" runat="server" RepeatColumns="4" DataSourceID="SqlDataSource1" OnItemCommand="ProductsData_ItemCommand" >
                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" />
                <ItemTemplate>
                    <div class="product">
                        <div class="image">
                            <asp:Image ID="Image1" runat="server" Height="100px" ImageUrl='<%# "data:image/jpg;base64," + Convert.ToBase64String((byte[])Eval("Image")) %>' Width="100px" />
                        </div>
                        <div class="description"><%#Eval("Name") %></div>

                        <div class="price"><%#Eval("Price")%>&euro;
                            <div>
                            <asp:Label class="quantLabel" runat="server"  Text="Quantity:"></asp:Label>
                            <asp:TextBox class="quantBox" ID="quantity" runat="server" name="quantity" Text="1"></asp:TextBox>
                            </div>
                        </div>
                        <asp:Button class="purchaseButton" ID="addtocart" runat="server" Text="Add To Cart" CommandArgument='<%#Eval("Id") %>' CommandName="addtocart" />
                        <br />
                        <asp:Label ID="Label1" runat="server" ForeColor="Red" Text="Error" Visible="False"></asp:Label>
                        <asp:LinkButton ID="LinkButton1" runat="server" CommandName="viewdetail" CommandArgument='<%#Eval("Id") %>' >See details</asp:LinkButton>
                        <br />
                    </div>
                </ItemTemplate>

			    </asp:DataList>
            </div>
        <div class="pageButtons">
            <asp:Button ID="Button1" runat="server" Text="<<" OnClick="Button1_Click"/>
            <asp:Button ID="Button2" runat="server" Text="<" OnClick="Button2_Click"/>
            <asp:Button ID="Button3" runat="server" Text=">" OnClick="Button3_Click"/>
            <asp:Button ID="Button4" runat="server" Text=">>" OnClick="Button4_Click"/>
        </div>
            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" SelectCommand="SELECT * FROM [Products]"></asp:SqlDataSource>

    </form>
</body>
</html>

