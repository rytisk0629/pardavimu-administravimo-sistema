﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PAS
{
    public partial class OrderDetails : System.Web.UI.Page
    {
		SqlConnection conn = new SqlConnection("Data Source=(LocalDB)\\MSSQLLocalDB;AttachDbFilename=|DataDirectory|\\PaasDB.mdf;Integrated Security=True");

		protected void Page_Load(object sender, EventArgs e)
        {
			string id = Request.QueryString["id"];
			string user = User.Identity.Name;
			conn.Open();
			string sql = "SELECT * FROM [orderitem] WHERE [orderitem].[orderid] = '" + id + "'";
			string sql1 = "SELECT [products].[name],[orderitem].[quantity],[orderitem].[orderid],[order].[orderdate] " +
					" FROM [dbo].[orderitem]" +
					" INNER JOIN [products] ON [products].[id] = [orderitem].[productid] AND [orderitem].[orderid] = '" + id + "'" +
					"INNER JOIN [order] ON [order].[id] = '" + id + "'";
			SqlCommand cmd = new SqlCommand(sql1, conn);
			cmd.ExecuteNonQuery();
			SqlDataAdapter adapter = new SqlDataAdapter();
			adapter.SelectCommand = cmd;
			DataSet ds = new DataSet();
			adapter.Fill(ds);
			OrderProducts.DataSource = ds;
			OrderProducts.DataBind();
			conn.Close();

		}
		protected void logout_click(object sender, EventArgs e)
		{
			FormsAuthentication.SignOut();
			Response.Redirect("~/Login.aspx");
		}
	}
}