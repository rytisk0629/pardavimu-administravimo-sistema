﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Web.Security;

namespace PAS
{
	public partial class MySales : System.Web.UI.Page
	{
		SqlConnection con = new SqlConnection("Data Source=(LocalDB)\\MSSQLLocalDB;AttachDbFilename=|DataDirectory|\\PaasDB.mdf;Integrated Security=True");
		protected void Page_Load(object sender, EventArgs e)
		{
				con.Open();
				string user = User.Identity.Name;
				string sql = "SELECT [products].[name],[orderitem].[quantity],[orderitem].[orderid],[order].[orderdate],[order].[address] " +
					" FROM [dbo].[orderitem]" +
					" INNER JOIN [products] ON [products].[id] = [orderitem].[productid] AND [products].[seller] = '" + user + "'" +
					"INNER JOIN [order] ON [order].[id] = [orderitem].[orderid]";

			SqlCommand com = new SqlCommand(sql, con);
				com.ExecuteNonQuery();
				SqlDataAdapter sda = new SqlDataAdapter();
				sda.SelectCommand = com;
				DataSet ds = new DataSet();
				sda.Fill(ds);
				SalesList.DataSource = ds;
				SalesList.DataBind();
				con.Close();
			
		}
		protected void logout_click(object sender, EventArgs e)
		{
			FormsAuthentication.SignOut();
			Response.Redirect("~/Login.aspx");
		}
	}
}