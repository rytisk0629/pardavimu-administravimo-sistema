﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="About.aspx.cs" Inherits="PAS.About" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>About Us</title>
    <link type="text/css" rel="stylesheet" href="Styles/About.css"/>
</head>
<body>
    <form id="form1" runat="server">
         <nav>
            <asp:Image runat="server" ImageUrl="~/Img/logo.png" CssClass="logoImg"/>
            <ul>
                <li><a class="LandingPage.aspx" href="LandingPage.aspx">Home</a></li>
                <li><a href="About.aspx">About</a></li>
                <li><a href="Login.aspx" onclick="window.open('Login.aspx', '_self');">Login</a></li>
                <li><a href="Register.aspx" onclick="window.open('Registration.aspx', '_self');">Register</a></li>
            </ul>
        </nav>
        <div class="info">
            <p>PAS is an easy to use, fast and free buying and selling platform. You can sell something you don't use anymore or maybe 
                your products that crafted yourself.
            </p>
            <br />
            <br />
            <p>Contacts: pas@gmail.com</p>
        </div>
        <asp:Image runat="server" ImageUrl="~/Img/shutterstock_569019595.jpg" CssClass="img" />
    </form>
</body>
</html>
