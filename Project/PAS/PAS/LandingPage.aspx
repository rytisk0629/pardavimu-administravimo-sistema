﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LandingPage.aspx.cs" Inherits="PAS.LandingPage" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>PAS</title>
    <link type="text/css" rel="stylesheet" href="Styles/LandingPage.css"/>
</head>
<body>
    <form id="form1" runat="server">
        <nav>
            <asp:Image runat="server" ImageUrl="~/Img/logo.png" CssClass="logoImg"/>
            <ul>
                <li><a class="active" href="#">Home</a></li>
                <li><a href="#">About</a></li>
                <li><a href="#" onclick="window.open('Login.aspx', '_self');">Login</a></li>
                <li><a href="#" onclick="window.open('Registration.aspx', '_self');">Register</a></li>
            </ul>
        </nav>
    </form>
    <div class="showcase">
    </div>
</body>
</html>
