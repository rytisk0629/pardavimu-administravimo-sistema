﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PlanOrder.aspx.cs" Inherits="PAS.PlanOrder" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>PAS</title>
    <link type="text/css" rel="stylesheet" href="Styles/LandingPage.css" media="screen" runat="server"/>
    <link type="text/css" rel="stylesheet" href="Styles/PlanOrder.css" media="screen" runat="server"/>
</head>
<body>
    <form id="form1" runat="server">
        <nav>
            <label class="logo">LOGO</label>
            <ul>
                <li><a class="active" href="#" onclick="window.open('LandingPage.aspx', '_self');">Home</a></li>
                <li><a href="#" onclick="window.open('ShoppingCart.aspx', '_self');">Cart</a></li>
                <li><a href="#" onclick="window.open('MyOrders.aspx', '_self');">Settings</a></li>
                <li><a href="#" onclick="window.open('MyOrders.aspx', '_self');">Logout</a></li>
            </ul>
        </nav>
		<div class="container-table100">
			<div class="wrap-table100">
				<div class="table100">
                    <asp:DropDownList id="Time" AutoPostBack="True" runat="server">
                        <asp:ListItem Selected="True" Value="Week"> Week </asp:ListItem>
                        <asp:ListItem Value="Month"> Month </asp:ListItem>
                        <asp:ListItem Value="Year"> Year </asp:ListItem>
                    </asp:DropDownList>
                    <h2>Orders</h2>
                    <table>
                        <thead>
                            <tr class="table100-head">
                                <th class="column1">Description</th>
                                <th class="column2">Amount</th>
                            </tr>
                        </thead>
                        <asp:Repeater ID="OrdersList" runat="server">
                            <ItemTemplate>   
                                    <tr>
                                        <td class="column1"><%#Eval("description") %></td>
                                        <td class="column2"><%#Eval("sum_amount") %></td>
                                    </tr>
                                </ItemTemplate>
                        </asp:Repeater>
                    </table>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
