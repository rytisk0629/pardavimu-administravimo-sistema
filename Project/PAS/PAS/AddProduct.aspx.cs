﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using System.Web.Security;

namespace PAS
{
    public partial class imagesUpload : System.Web.UI.Page
    {
        SqlConnection con = new SqlConnection("Data Source=(LocalDB)\\MSSQLLocalDB;AttachDbFilename=|DataDirectory|\\PaasDB.mdf;Integrated Security=True");
        protected void Page_Load(object sender, EventArgs e)
        {
            con.Open();
        }

        protected void submit_Click1(object sender, EventArgs e)
        {
            bool nofile = false;
            bool blank = false;
            bool priceChar = false;
            char[] allowedPrice = { '.', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };
            string allowed = ".0123456789";

            if (!fileImage.HasFile)
            {
                nofile = true;
                errorLabel.Text = "Product image missing";
                errorLabel.Visible = true;
            }
            foreach (char c in price.Text)
            {
                if (!allowed.Contains(c.ToString()))
                {
                    priceChar = true;
                    errorLabel.Text = "Price field contains unallowed characters";
                    errorLabel.Visible = true;
                    break;
                }
            }

            if (name.Text == String.Empty)
            {
                blank = true;
                errorLabel.Text = "Some fields are blank";
                errorLabel.Visible = true;
            }
            if (brand.Text == String.Empty)
            {
                blank = true;
                errorLabel.Text = "Some fields are blank";
                errorLabel.Visible = true;
            }
            if (price.Text == String.Empty)
            {
                blank = true;
                errorLabel.Text = "Some fields are blank";
                errorLabel.Visible = true;
            }
            if (Request.Form["description"] == String.Empty)
            {
                blank = true;
                errorLabel.Text = "Some fields are blank";
                errorLabel.Visible = true;
            }

            if (nofile == false && blank == false && priceChar == false)
            {
                int len = fileImage.PostedFile.ContentLength;
                byte[] pic = new byte[len + 1];
                fileImage.PostedFile.InputStream.Read(pic, 0, len);
                string user = User.Identity.Name;
                string str = "insert into Products values(@Name, @Category, @Description,@Brand,@Price,@Image,@Seller)";
                SqlCommand com = new SqlCommand(str, con);
                string desc = Request.Form["description"];
                com.Parameters.AddWithValue("@Name", name.Text);
                com.Parameters.AddWithValue("@Category", Convert.ToInt32(categoryList.SelectedValue));
                com.Parameters.AddWithValue("@Description", description.Text);
                com.Parameters.AddWithValue("@Brand", brand.Text);
                com.Parameters.AddWithValue("@Price", price.Text);
                com.Parameters.AddWithValue("@Image", pic);
                com.Parameters.AddWithValue("@Seller", user);
                com.ExecuteNonQuery();
                con.Close();
                Response.Redirect("~/MyProducts.aspx");
            }
        }

        protected void goBack_Click(object sender, EventArgs e)
        {
            Response.Redirect("ProductsCatalogue.aspx");
        }
        protected void logout_click(object sender, EventArgs e)
        {
            FormsAuthentication.SignOut();
            Response.Redirect("~/Login.aspx");
        }
    }
}